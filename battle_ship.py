import random
import numpy


board = []

def printBoard(board):
  for row in board:
    print(' '.join(row))

print("Welcome to BATTLESHIP")
turns = 0
for i in range(5):
  board.append(["[]"] * 5)


while(turns<5):
    row = random.randint(0,4)
    col = random.randint(0,4)
    userRow = int(input("Enter row"))
    userCol = int(input("Enter col"))
    if((userRow > 4 or userRow <0) or (userCol>4 or userCol < 0)):
        print("Invalid coordinates! Try again")
        continue
    printBoard(board)
    board[row][col] = "*"
    turns+=1
    board[userRow][userCol] = "X"
    if(board[userRow][userCol]== board[row][col]):
        print("Hit at ", userRow, userCol)
    else:
        print("Miss")
    printBoard(board)
